/**
 * New node file
 */

var fs = require('fs');
var path = require('path');
//Set the global path
global.PROJ_ROOT = path.resolve(__dirname);



var Util = require(global.PROJ_ROOT + "/utils/utils");

var labelFd = fs.openSync("res/train-labels.raw" , "r");
var fd = fs.openSync("res/train.raw" , "r");




var headerSize = 4 + 4 + 4 + 4;
var labelHeaderSize = 8;

var Network = require("./model/Network");

//var trainingSet = [
//                   { 
//                	   	inputs : [0 , 0 , 1] , 
//						expectedOutput: [1]
//                   } ,
//                   { 
//               	   		inputs : [0, 0 , 1] , 
//						expectedOutput: [1]
//                   } ,
//                   { 
//              	   		inputs : [1, 0 , 0] , 
//						expectedOutput: [0]
//                   },
//                   { 
//             	   		inputs : [0, 1 , 0] , 
//						expectedOutput: [0]
//                   },
//                   { 
//             	   		inputs : [1, 1 , 0] , 
//						expectedOutput: [0]
//                   } ,
//                   { 
//               	   		inputs : [0 , 0 , 1] , 
//						expectedOutput: [1]
//                  } ,
//                  { 
//              	   		inputs : [0, 0 , 1] , 
//						expectedOutput: [1]
//                  } ,
//                  { 
//             	   		inputs : [1, 0 , 0] , 
//						expectedOutput: [0]
//                  },
//                  { 
//            	   		inputs : [0, 1 , 0] , 
//						expectedOutput: [0]
//                  },
//                  { 
//            	   		inputs : [1, 1 , 0] , 
//						expectedOutput: [0]
//                  }
//                   ];
//
//var testingSet = [{ 
//		inputs : [1, 1 , 0]
//  }]

var trainingSet = [];
var testingSet = [];


var trainingRatio = 0.8;


readFile(fd , 0 , 10000 , function()
{
	console.log("File read successfully");	
});



function readFile(fd , imgCount , limit , callback)
{
	for (var readFile = imgCount; readFile < limit ; readFile ++)
	{
		var input = [];
		var row = 28 , col = 28;
		
		var buffer = new Buffer(row * col);
		
		fs.readSync(fd, buffer, 0, row * col, headerSize + readFile * col * row);
		
		var pointer = 0;
		
		for (var i = 0; i < row; i ++)
		{
			for (var j = 0; j < col ; j ++)
			{   
				var byte =  buffer.readUInt8(pointer);
				input.push(byte > 0 ? 1 : 0)
				pointer ++ ;
				//image.setPixel(i , j , readByte(buffer));
			}
		}
		
		
		var inputObj = {};
		inputObj.input = input;
		inputObj.output = [0 , 0, 0 , 0 , 0 , 0 , 0 , 0, 0 , 0];
		var num = readLabel(readFile);
		inputObj.output[num] = 1;
		
		
		trainingSet.push(inputObj);
		testingSet.push(inputObj);
		
//		if (Util.random(0 , 1) < trainingRatio)
//		{
//			trainingSet.push(inputObj);
//		} else 
//		{
//			testingSet.push(inputObj);
//		}
			
	}
	
	// close the files
	fs.close(fd);
	fs.close(labelFd);
	
	callback();
	
	
}

function readLabel(imgCount)
{
	var labelBuffer = new Buffer(1);
	fs.readSync(labelFd, labelBuffer, 0, 1 , labelHeaderSize + imgCount);
	
	return labelBuffer.readUInt8(0);	
}




var network = new Network([28 * 28 , 30  ,10] , trainingSet , testingSet);
//network.info();
for (var i = 0; i < 50; i ++)
{
	console.log("Epoch #" + i);
	
	network.train(10000);
	
}

network.test(10000);

// accurate , but super slow...

//var brain = require("brain");
//
//var net = new brain.NeuralNetwork();
//
//net.train(trainingSet , {
//	iterations : 10 ,
//	log : true ,
//	logPeriod : 1 ,
//	hiddenLayers : [10]
//});
//
//for (var i = 0; i < 10 ; i ++)
//{
//	console.log("Epoch #" + i);
//	var output = net.run(testingSet[i].input);
//	console.log("Output :" + output);
//	console.log("Expected :" + testingSet[i].output);
//	console.log();
//}