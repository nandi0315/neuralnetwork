/**
 * New node file
 */


/*
 * Random a number which lies in [low, high)
 */
function random(low , high)
{
	return Math.random() * (high - low) + low;
}

module.exports = {
		"random" : random
}