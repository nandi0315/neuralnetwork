
var Util = require(global.PROJ_ROOT + "/utils/utils");


function Neuron(inputCount)
{
	this.weights = [];
	this.bias = Util.random(0 , 1);
	this.error = 0.0;
	this.calculatedOutput = 0.0;
	
	this.inputs = [];
	
	for (var i = 0 ; i < inputCount ; i ++)
	{
		// Init the weights 
		this.weights.push(Util.random(-0.5 , 0.5));
	}
}

Neuron.prototype.feedForward = function(inputs)
{
	var sum = 0;
	this.inputs = inputs;
	
	if (inputs.length != this.weights.length)
	{
		throw new Error("Weights and inputs not match");
	}
	
	
	for (var i = 0; i < inputs.length; i ++)
	{
		sum += this.weights[i] * inputs[i];
	}
	
	sum += this.bias;
	
	this.calculatedOutput = sigmoid(sum)
	return this.calculatedOutput;
}

Neuron.prototype.setError = function(error)
{
	this.error = error;
}

Neuron.prototype.getError = function()
{
	return this.error;
}



Neuron.prototype.getOutput = function()
{
	return this.calculatedOutput;
}


Neuron.prototype.getWeight = function(index)
{
	return this.weights[index];
}

Neuron.prototype.updateWeight = function(learningRate)
{
	for (var i = 0 ; i < this.weights.length; i ++)
	{
		this.weights[i] = this.weights[i] + this.inputs[i] * this.error * learningRate;
	}
	this.bias = this.bias +  this.error * learningRate;
}



function sigmoid(value)
{
	return 1.0/ (1.0 + Math.exp(-value));
}


Neuron.prototype.activate = function()
{
	return this.calculatedOutput > 0.5 ? 1 : 0;
}

Neuron.prototype.info = function()
{
	var info = "Bias=" + this.bias + " , Weights=[";
	for (var i = 0 ; i < this.weights.length; i ++)
	{
		info += this.weights[i] + ",";
	}
	
	info += "]";
	
	info += ", error=" + this.error + ", inputs=" + this.inputs + ",output=" + this.calculatedOutput;
	console.log(info);
}

module.exports = Neuron;