/**
 * New node file
 */

var Neuron = require("./neuron");
var Util = require(global.PROJ_ROOT + "/utils/utils");

function Network(layers , trainingSet , testingSet , learningRate)
{
	// 2D arrays , i = layer
	this.neurons = [];
	
	this.trainingSet = trainingSet;
	this.testingSet = testingSet;
	
	this.learningRate = learningRate;
	
	this.layers = layers;
	
	for (var i = 0 ; i < layers.length; i ++)
	{
		var neuronCount = layers[i];
		var lastLayerCount = i == 0? 0 : layers[i - 1];
		
		this.neurons[i] = [];
		
		
		for (var count =0 ; count < neuronCount; count++)
		{
			this.neurons[i].push(new Neuron(lastLayerCount));
		}
	}
	
}
Network.prototype.train = function(size)

{
	shuffle(this.trainingSet);
	
	for (var i = 0; i < size ; i ++) 
	{
		this.inputs = this.trainingSet[i].input;
		this.expectedOutput = this.trainingSet[i].output;
		this.activate();
		this.backprop();
		this.updateWeights();
	}
	
}

Network.prototype.run = function(input)
{
	var input = input;
	for (var i = 1; i < this.layers.length; i ++)
	{
		var neuronCount = this.layers[i];		
		var output = [];
		
		for (var count =0 ; count < neuronCount; count++)
		{
			var neuron = this.neurons[i][count];
			var sigmoid = neuron.feedForward(input);
			output.push(sigmoid);	
			
		}
		
		// prepare the next layer
		input = output;
	}
	
	return input;
}


// Shuffle the array
function shuffle(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Network.prototype.test = function(size)
{
	shuffle(this.testingSet);
	
	var total = 0;
	var correct = 0;
	//for (var i = 0; i < this.testingSet.length; i ++)
	for (var i = 0; i < size ; i ++)
	{
		this.inputs = this.testingSet[i].input;
		this.expectedOutput = this.testingSet[i].output;
		this.activate();
		
		var match = true;
		var output = [];
		
		for (var j = 0; j < this.expectedOutput.length; j ++)
		{	
			output.push(this.neurons[this.layers.length - 1][j].getOutput());
			if (this.expectedOutput[j] != this.neurons[this.layers.length - 1][j].activate())
			{
				//console.log("Not match");
				match = false;
			}
		}
		
		//console.log("Expected Output :" + this.expectedOutput + " vs Calculated Output :" + output);
		
		if (match)
		{
			correct ++;
		}
		
		total ++;
	}
	
	console.log("Success rate : " + correct  + "/" + total);
}

Network.prototype.activate = function()
{
	var input = this.inputs;
	
	for (var i = 1; i < this.layers.length; i ++)
	{
		var neuronCount = this.layers[i];		
		var output = [];
		
		for (var count =0 ; count < neuronCount; count++)
		{
			var neuron = this.neurons[i][count];
			var sigmoid = neuron.feedForward(input);
			output.push(sigmoid);	
			
		}
		
		// prepare the next layer
		input = output;
	}
	
	
	// https://www4.rgu.ac.uk/files/chapter3%20-%20bp.pdf
	// http://www.cleveralgorithms.com/nature-inspired/neural/backpropagation.html
}



/*
 * if index == network.size-1
      neuron = network[index][0] # assume one node in output layer
      error = (expected_output - neuron[:output])
      neuron[:delta] = error * transfer_derivative(neuron[:output])
    else
      network[index].each_with_index do |neuron, k|
        sum = 0.0
        # only sum errors weighted by connection to the current k'th neuron
        network[index+1].each do |next_neuron|
          sum += (next_neuron[:weights][k] * next_neuron[:delta])
        end
        neuron[:delta] = sum * transfer_derivative(neuron[:output])
      end
 */

/*
 * Calculate the error
 */
Network.prototype.backprop = function()
{
	// expectedOutput
	for (var i = this.layers.length - 1; i > 0 ; i -- )
	{		
		// output layer
		if (i == this.layers.length - 1)
		{	
			var outputNeurons = this.neurons[i];
			for (var j = 0; j < outputNeurons.length; j ++)
			{
				var outputNeuron = outputNeurons[j];
				var error = outputNeuron.getOutput() * (1 - outputNeuron.getOutput()) * (this.expectedOutput[j] - outputNeuron.getOutput());
				outputNeuron.setError(error);
				//console.log("Expected Output :" + this.expectedOutput[j] + " vs Calculated Output :" + outputNeuron.getOutput());
				//console.log("Error = " + error);
				//outputNeuron.info();
			}
			
		}else
		{
			var hiddenNeurons = this.neurons[i];
			for (var j = 0; j < hiddenNeurons.length; j ++)
			{
				var hiddenNeuron = hiddenNeurons[j];
				var perviousLayerNeruons = this.neurons[i + 1];
				var error = 0.0;
				for (var k = 0; k < perviousLayerNeruons.length; k ++)
				{
					error += hiddenNeuron.getWeight(k) * perviousLayerNeruons[k].getError();
				}
				error = error * hiddenNeuron.getOutput() * (1 - hiddenNeuron.getOutput());
				hiddenNeuron.setError(error);
			}
		}
	}	
}


Network.prototype.updateWeights = function()
{
	// Except the input layers
	for (var i = this.layers.length - 1; i > 0 ; i -- )
	{		
		// output layer
		if (i == this.layers.length - 1)
		{	
			var outputNeurons = this.neurons[i];
			for (var j = 0; j < outputNeurons.length; j ++)
			{
				var outputNeuron = outputNeurons[j];
				outputNeuron.updateWeight(this.learningRate);
				//outputNeuron.info();
			}
			
		}else
		{
			var hiddenNeurons = this.neurons[i];
			for (var j = 0; j < hiddenNeurons.length; j ++)
			{
				var hiddenNeuron = hiddenNeurons[j];
				hiddenNeuron.updateWeight(this.learningRate);
			}
		}
	}	
}

Network.prototype.info = function()
{
	
	for (var i = 0; i < this.neurons.length; i ++)
	{
		console.log("Layer : " + i);
		for (var j = 0; j < this.neurons[i].length; j ++)
		{
			this.neurons[i][j].info();
		}
	}
		
}

module.exports = Network;