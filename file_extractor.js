/**
 * @author Nandi @ 2015-06-24
 * Extract the MNIST data files
 */

var fs = require('fs');



var bmp = require("./bmpEncoder");
var headerSize = 4 + 4 + 4 + 4;

var labelFd = fs.openSync("img/train-labels.raw" , "r");
var fd = fs.openSync("img/train.raw" , "r");


var headerBuffer = new Buffer(headerSize);
var headerOffset = 0;
var labelHeaderSize = 8;
var imgCounter = 0;
var labelHash = [];


labelHash = [];

fs.read(fd, headerBuffer, 0, headerSize, 0, function(err, num) 
{
	var buffer = {buffer : headerBuffer , offset: headerOffset};
	console.log("Magic number : " + readInt(buffer));
	console.log("Number of images : " + readInt(buffer));
	var row = readInt(buffer);
	var col = readInt(buffer);
	
	readFile(fd , 0 , 20000);
	
});


function readLabel(imgCount)
{
	var labelBuffer = new Buffer(1);
	fs.readSync(labelFd, labelBuffer, 0, 1 , labelHeaderSize + imgCount);
	var bufferObj = {buffer : labelBuffer , offset: 0};
	
	return readByte(bufferObj);
}


function readFile(fd , imgCount , limit)
{
	var row = 28 , col = 28;
	
	var buffer = new Buffer(row * col);
	
	fs.readSync(fd, buffer, 0, row * col, headerSize + imgCount * col * row);
	
	var imageBuffer = new Buffer(row * col);
	var imageBufferPtr = 0;
	var bufferOffset = 0;
	
	var bufferObj = {buffer : buffer , offset: bufferOffset};
	for (var i = 0; i < row; i ++)
	{
		for (var j = 0; j < col ; j ++)
		{   
			var byte = readByte(bufferObj);
			imageBuffer.writeUInt8(byte ,imageBufferPtr);
			imageBufferPtr ++;
			//image.setPixel(i , j , readByte(buffer));
		}
	}
	
	
	var rawData = bmp.encode({data : imageBuffer , width : col , height : row});
	
	
	//console.log("image data read. size:" + row + "x" + col);
	fs.writeFileSync(getImageNameByIndex(imgCount) , rawData.data);	
	
	if (imgCount < limit)
	{
		readFile(fd , imgCount + 1 , limit);
	}else
	{
		// close the files
		fs.close(fd);
		fs.close(labelFd);
	}
}

function getImageNameByIndex(imgCount)
{
	var label =  readLabel(imgCount);
	
	if (labelHash[label] == undefined)
	{
		labelHash[label] = 0;
	}
	
	
	
	return 'img/' + label + "_" +  ++labelHash[label] + '.bmp';
}

function readInt(buffer)
{	
	var data = buffer.buffer.readInt32BE(buffer.offset);
	buffer.offset += 4;
	return data;
}


function readByte(buffer)
{
	
	var data = buffer.buffer.readUInt8(buffer.offset);
	buffer.offset += 1;
	return data;
}

