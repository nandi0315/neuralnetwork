/**
 * New node file
 */


var request = require('request');
var fs = require('fs');

var cookieString = "__AF=498bc065-7746-438e-b0a1-7f12cc42bb64; Remember=rmps3djbp0ypm0pzt2o0hitm; ddddddd=29e4a3f6ddddddd_29e4a3f6; CultureInfo=en-US; RegionId=0; __utma=183676536.429167542.1424769110.1435122296.1435570994.19; __utmb=183676536.8.10.1435570995; __utmc=183676536; __utmz=183676536.1432635536.12.7.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided)";

// Map Zoom (smaller = wider)

function getUrl(pageSize , pageIndex)
{
	return "http://www.openrice.com/api/api.htm?method=or.poi.getlistbymap&api_token=apitoken&api_sig=apisignature&response_type=json&ret_info=addr,popularTag,promotion,default&page=" + pageIndex + "&per_page=" + pageSize + "&mapsearch=1&region_id=0&normal=1&maplat=22.280209&maplng=114.18411600000002&mapzoom=17&wxh=1903x420";
}

function getUrlByLocation(lat , long)
{
	return "http://www.openrice.com/api/api.htm?method=or.poi.getlistbymap&api_token=apitoken&api_sig=apisignature&response_type=json&ret_info=addr,popularTag,promotion,default&page=0&per_page=500&mapsearch=1&region_id=0&normal=1&maplat=" + lat + "&maplng="+ long + "&mapzoom=19&wxh=1903x420";
}

//var j = request.jar();
//var cookie = request.cookie(cookieString);
//console.log(cookie);
//
//j.setCookie(cookie, url , function(){});

var startLat = 22.276761793582767;
var startLong = 114.1777052471237;

var endLat = 22.280761793582767;
var endLong = 114.1847052471237;

var offset = 0.001;

//loadRequest(1000 , 0);
loadRequestByLocation(startLat , startLong);

var restaurants = {};


function loadRequestByLocation(lat , long)
{
	var options = {
			url : getUrlByLocation(lat , long),
			headers: {
				//'Cookie': cookieString,
				'Referer' : 'http://www.openrice.com/en/hongkong/restaurant?region=0&mapType=1'
			}
	}
	
	request(options , function (req , res , err)
			{
				var json = JSON.parse(res.body);
				var data = json.Root.Data[0].Pois[0];
				
				var count = data.count;
				var limit = data.limit;
				var page = data.page;
				//console.log(data);
				console.log("Count :" + count);
				console.log("Limit :" + limit);
				console.log("Page  :" + page);
				if (count > 0)
				{
					for (var i in data.Poi)
					{
						var restaurant = data.Poi[i];
						restaurants[restaurant.Id] = restaurant;
					}
					
				}
				
				// End condition
				if (lat >= endLat && long >= endLong)
				{
					console.log(restaurants.length + " restaurants loaded");
					fs.writeFileSync("data/restaurant.json" , JSON.stringify(restaurants , null, '\t'));
					
				}else
				{
					if (long >= endLong)
					{
						loadRequestByLocation(lat + offset , startLong) ;
						
					}else
					{
						loadRequestByLocation(lat , long + offset) ;
					}
				}
				//var restaurants = data[0].Pois[0].Poi;
				//console.log(restaurants);
				
				//fs.writeFileSync("data/restaurant.json" , JSON.stringify(restaurants , null, '\t'));	
			})
}


function loadRequest(pageSize , pageIndex)
{
	var options = {
			url : getUrl(pageSize , pageIndex),
			headers: {
				//'Cookie': cookieString,
				'Referer' : 'http://www.openrice.com/en/hongkong/restaurant?region=0&mapType=1'
			}
	}
	
	request(options , function (req , res , err)
			{
				var json = JSON.parse(res.body);
				var data = json.Root.Data[0].Pois[0];
				
				var count = data.count;
				var limit = data.limit;
				var page = data.page;
				//console.log(data);
				console.log("Count :" + count);
				console.log("Limit :" + limit);
				console.log("Page  :" + page);
				if (count > 0)
				{
					restaurants = restaurants.concat(data.Poi);
				}
				
				// End condition
				if (count >= pageSize)
				{
					loadRequest(pageSize , pageIndex + 1);
				}else
				{
					console.log(restaurants.length + " restaurants loaded");
					fs.writeFileSync("data/restaurant.json" , JSON.stringify(restaurants , null, '\t'));	
				}
				//var restaurants = data[0].Pois[0].Poi;
				//console.log(restaurants);
				
				//fs.writeFileSync("data/restaurant.json" , JSON.stringify(restaurants , null, '\t'));	
			})
}

