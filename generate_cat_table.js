/**
 * Parse the restaurant and list all the categories
 */


var fs = require('fs');
var path = require('path');
//Set the global path
global.PROJ_ROOT = path.resolve(__dirname);



var Util = require(global.PROJ_ROOT + "/utils/utils");

var data = fs.readFileSync("data/restaurant.json");
var restaurants = JSON.parse(data);

var keys = Object.keys(restaurants)


var categoryKeys = {};

try
{
	var categoryKeyString = fs.readFileSync("data/categories.json");
	categoryKeys = JSON.parse(categoryKeyString);
	
	console.log("File loaded");
	console.log(categoryKeys);
}catch (error)
{
}




for (var i = 0; i < keys.length; i ++)
{
	var restaurant = restaurants[keys[i]];
	for (var j in restaurant.Categories[0].Category)
	{
		var cat = restaurant.Categories[0].Category[j];
		var id = cat.Id;
		
		if (categoryKeys[id] == undefined)
		{
			categoryKeys[id] = Object.keys(categoryKeys).length;
		}
	}
}

fs.writeFileSync("data/categories.json" , JSON.stringify(categoryKeys));
