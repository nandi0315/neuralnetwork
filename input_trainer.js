/**
 * New node file
 */

var fs = require('fs');
var path = require('path');
//Set the global path
global.PROJ_ROOT = path.resolve(__dirname);



var Util = require(global.PROJ_ROOT + "/utils/utils");

var inputData = fs.readFileSync("res/input.data" , "utf-8");

var rows = inputData.split("\r\n");
var records = [];
var parsedData = [];

var categoryKeyString = fs.readFileSync("data/categories.json");
var categoriesIndex = JSON.parse(categoryKeyString);

var inputSize = 20 + Object.keys(categoriesIndex).length;


var restaurantData = fs.readFileSync("data/restaurant.json");
var restaurants = JSON.parse(restaurantData);



for (var i = 1 ; i < rows.length; i ++)
{
	// Read the line
	
	var row = rows[i];
	// format:
	// id:178003,result=false,price=10,distance=0.24535586966374484,smile=1,cat=1004|1013
	
	var tokens = row.split(",");
	var data = {};
	for (var j in tokens)
	{
		var token = tokens[j];
		var pair = token.split("=");
		if (pair < 2)
			continue;
		
		
		
		if (pair[0] == "cat")
		{
			data[pair[0]] = pair[1].split("|");
		}else
		{
			data[pair[0]] = pair[1];
		}		
		
	}
	
	parsedData.push(data);
}


var trainingData = [];

// Ok , here we will normalize the input and prepare for training
for (var i = 1 ; i < parsedData.length; i ++)
{
	var record = parsedData[i];
	
	var input = zeros(inputSize);
	
	var price = parseInt(record.price);
	var distance = parseInt(record.distance);
	var smile = parseInt(record.smile);
	
	// 0 - 2 : price
	input[0] = price < 50 ? 1 : 0;
	input[1] = price >=50 && price < 100? 1 : 0;
	input[2] = price > 100 ? 1 : 0;
	
	input[3] = distance < 0.5 ? 1 : 0;
	input[4] = distance >=0.5 && distance < 1? 1 : 0;
	input[5] = distance >=1 && distance < 2? 1 : 0;
	input[6] = distance > 2 ? 1 : 0;
	
	if (isNaN(smile))
		smile = 1;
	
	input[7] = smile < 0.3 ? 1 : 0;
	input[8] = smile >=0.3 && smile < 0.6? 1 : 0;
	input[9] = smile >=0.6 && smile < 0.8? 1 : 0;
	input[10] = smile >=0.8 ? 1 : 0;
	
	if (record.cat != undefined)
	{
		for (var j = 0 ; j < record.cat.length; j ++)
		{
			if (categoriesIndex[record.cat[j]] == undefined)
				continue;
			
			var index = categoriesIndex[record.cat[j]];
			input[20 + index] = 1;
		}
	}
	
	var data = {};
	data.input = input;
	data.output = [];
	data.output.push(record.result == "true" ? 1 : 0);
	
	trainingData.push(data);
}

function zeros(size)
{
	var array = new Array(size);
	for (var i = 0; i < size; i ++)
		array[i] = 0;
	return array;
}



var headerSize = 4 + 4 + 4 + 4;
var labelHeaderSize = 8;

var Network = require("./model/Network");

//var trainingSet = [
//                   { 
//                	   	inputs : [0 , 0 , 1] , 
//						expectedOutput: [1]
//                   } ,
//                   { 
//               	   		inputs : [0, 0 , 1] , 
//						expectedOutput: [1]
//                   } ,
//                   { 
//              	   		inputs : [1, 0 , 0] , 
//						expectedOutput: [0]
//                   },
//                   { 
//             	   		inputs : [0, 1 , 0] , 
//						expectedOutput: [0]
//                   },
//                   { 
//             	   		inputs : [1, 1 , 0] , 
//						expectedOutput: [0]
//                   } ,
//                   { 
//               	   		inputs : [0 , 0 , 1] , 
//						expectedOutput: [1]
//                  } ,
//                  { 
//              	   		inputs : [0, 0 , 1] , 
//						expectedOutput: [1]
//                  } ,
//                  { 
//             	   		inputs : [1, 0 , 0] , 
//						expectedOutput: [0]
//                  },
//                  { 
//            	   		inputs : [0, 1 , 0] , 
//						expectedOutput: [0]
//                  },
//                  { 
//            	   		inputs : [1, 1 , 0] , 
//						expectedOutput: [0]
//                  }
//                   ];
//
//var testingSet = [{ 
//		inputs : [1, 1 , 0]
//  }]





var network = new Network([inputSize , 10  , 1] , trainingData , trainingData , 1.0);
//network.info();
for (var i = 0; i < 1000 ; i ++)
{
	console.log("Epoch #" + i);
	
	network.train(rows.length - 2);
	
}

network.test(rows.length - 2);

var keys = Object.keys(restaurants);
var currentLat = 22.277163;
var currentLong = 114.182177;

for (var i = 0; i < keys.length; i ++)
{
	var restaurant = restaurants[keys[i]];
	
	input = zeros(inputSize);
	
	var price = parseInt(restaurant.Price);
	var distance = calDistance(restaurant.MapLatitude , restaurant.MapLongitude , currentLat , currentLong , "KM");
	var smile = (parseInt(restaurant.ScoreSmile) * 1.0/(parseInt(restaurant.ScoreSmile) + parseInt(restaurant.ScoreOK) + parseInt(restaurant.ScoreCry)));
	if (isNaN(smile))
		smile = 1;
	
	
	// 0 - 2 : price
	input[0] = price < 50 ? 1 : 0;
	input[1] = price >=50 && price < 100? 1 : 0;
	input[2] = price > 100 ? 1 : 0;
	
	input[3] = distance < 0.5 ? 1 : 0;
	input[4] = distance >=0.5 && distance < 1? 1 : 0;
	input[5] = distance >=1 && distance < 2? 1 : 0;
	input[6] = distance > 2 ? 1 : 0;
	
	input[7] = smile < 0.3 ? 1 : 0;
	input[8] = smile >=0.3 && smile < 0.6? 1 : 0;
	input[9] = smile >=0.6 && smile < 0.8? 1 : 0;
	input[10] = smile >=0.8 ? 1 : 0;
	
	if (restaurant.Categories[0].Category != undefined)
	{
		
		for (var j = 0 ; j < restaurant.Categories[0].Category.length; j ++)
		{
			
			if (categoriesIndex[restaurant.Categories[0].Category[j].Id] == undefined)
				continue;
			
			var index = categoriesIndex[restaurant.Categories[0].Category[j].Id];
			//console.log(index);
			input[20 + index] = 1;
		}
		
		
	}
	
	restaurant.score = network.run(input);
	if (restaurant.score > 0.5)
	{
		//console.log(restaurant);
	}
}

fs.writeFileSync("data/restaurant_score.json" , JSON.stringify(restaurants , null, '\t'));


function calDistance(lat1, lon1, lat2, lon2, unit)
{
	var radlat1 = Math.PI * lat1/180
	var radlat2 = Math.PI * lat2/180
	var radlon1 = Math.PI * lon1/180
	var radlon2 = Math.PI * lon2/180
	var theta = lon1-lon2
	var radtheta = Math.PI * theta/180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist)
	dist = dist * 180/Math.PI
	dist = dist * 60 * 1.1515
	// calcualte in km
	dist = dist * 1.609344;
	return dist
} 

