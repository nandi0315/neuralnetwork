/**
 * New node file
 */

var express = require('express');
var app = express();

var ejs = require("ejs");
var fs = require("fs");

var path = require('path');
//Set the global path
global.PROJ_ROOT = path.resolve(__dirname);

app.set('view engine', 'ejs');


var data = fs.readFileSync("../data/restaurant_score.json");
var restaurants = JSON.parse(data);



app.get('/', function(req, res){
  
	var restaurant = randomRestaurant();
	console.log(restaurant);
	
	//res.send('hello world');
	res.render("index" , { title: 'Express' , restaurant : restaurant} );
});


app.get('/submit', function(req, res){
	  
	var result = req.query.result;
	var id = req.query.id;
	var distance = req.query.distance;
	var price = req.query.price;
	var categories = req.query.categories;
	var smileRatio = req.query.smileRatio;
	
	var restaurant = randomRestaurant();
	var row = getRow(id , result , price , distance , smileRatio , categories);
	
	fs.appendFileSync("../res/input.data" , row);
	//res.send('hello world');
	res.render("index" , { title: 'Express' , restaurant : restaurant} );
});

app.get('/recommend', function(req, res){
	  
	data = fs.readFileSync("../data/restaurant_score.json");
	restaurants = JSON.parse(data);
	
	var goodRests = getGoodRestaurants();
	
	//res.send('hello world');
	res.render("recommends" , { title: 'Express' , restaurants : goodRests} );
});

function getRow(id , result , price , distance , smileRatio , categories)
{
//	var string = id + "," + (result=="true"?1 : 0) + "," + 
//				(parseInt(price) < 50 ?1 : 0) + "," +
//				(parseInt(price) >= 50 && parseInt(price) < 100 ?1 : 0) + "," +
//				(parseInt(price) > 50 ?1 : 0) + "," +
//				(parseFloat(distance) < 0.5 ?1 : 0) + "," +
//				(parseFloat(price) >= 0.5 && parseFloat(price) < 1?1 : 0) + "," +
//				(parseFloat(price) > 1 ?1 : 0) + "," +
//				(parseFloat(smileRatio) <0.5 ?1 : 0) + "," +
//				(parseFloat(smileRatio) >=0.5 ?1 : 0) + "\r\n";
//
//	return string;	
	
	var string = "id:" + id + ",result=" + result +",price=" + price +",distance=" + distance + ",smile=" + smileRatio +",cat=" + categories +"\r\n";
	return string;
}


function getGoodRestaurants()
{
	var array = [];
	var keys = Object.keys(restaurants);
	
	for (var i = 0 ; i < keys.length; i ++)
	{
		var rest = restaurants[keys[i]];
		if (rest.score[0] > 0.5)
		{
			array.push(rest);
		}
	}
	
	return array.sort(compare);
}

function compare(a,b) {
	  if (a.score[0] < b.score[0])
	    return 1;
	  if (a.score[0] > b.score[0])
	    return -1;
	  return 0;
	}
function randomRestaurant()
{
	var keys = Object.keys(restaurants)
    return restaurants[keys[ keys.length * Math.random() << 0]];
	
//	var index = Math.floor(Math.random() * restaurants.length);
//	return restaurants[index];
}
app.listen(3000);