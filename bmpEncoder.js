/**
 * @author nandi
 *
 * BMP format encoder,encode 8bit BMP
 * Not support quality compression
 * 
 */

function BmpEncoder(imgData)
{
	this.buffer = imgData.data;
	this.width = imgData.width;
	this.height = imgData.height;
	this.extraBytes = 0;
	this.rgbSize = this.height*this.width + this.extraBytes;
	this.headerInfoSize = 40 ;

	this.data = [];
	/******************header***********************/
	this.flag = "BM";
	this.reserved = 0;
	this.offset = 54 + 256 * 4; // 256 for palette
	this.fileSize = this.rgbSize+this.offset;
	this.planes = 1;
	this.bitPP = 8;
	this.compress = 0;
	this.hr = 0;
	this.vr = 0;
	this.colors = 0;
	this.importantColors = 0;
	
	this.palette = [];
	
	for (var i = 0; i < 256 ; i ++)
	{
		this.palette.push({r:i , g:i , b:i , a:0});
	}
}

BmpEncoder.prototype.encode = function() {
	var tempBuffer = new Buffer(this.offset+this.rgbSize);
	this.pos = 0;
	tempBuffer.write(this.flag,this.pos,2);this.pos+=2;
	tempBuffer.writeUInt32LE(this.fileSize,this.pos);this.pos+=4;
	tempBuffer.writeUInt32LE(this.reserved,this.pos);this.pos+=4;
	tempBuffer.writeUInt32LE(this.offset,this.pos);this.pos+=4;

	tempBuffer.writeUInt32LE(this.headerInfoSize,this.pos);this.pos+=4;
	tempBuffer.writeUInt32LE(this.width,this.pos);this.pos+=4;
	tempBuffer.writeUInt32LE(this.height,this.pos);this.pos+=4;
	tempBuffer.writeUInt16LE(this.planes,this.pos);this.pos+=2;
	tempBuffer.writeUInt16LE(this.bitPP,this.pos);this.pos+=2;
	tempBuffer.writeUInt32LE(this.compress,this.pos);this.pos+=4;
	//tempBuffer.writeUInt32LE(this.rgbSize,this.pos);this.pos+=4;
	tempBuffer.writeUInt32LE(0x00,this.pos);this.pos+=4;
	tempBuffer.writeUInt32LE(this.hr,this.pos);this.pos+=4;
	tempBuffer.writeUInt32LE(this.vr,this.pos);this.pos+=4;
	tempBuffer.writeUInt32LE(this.colors,this.pos);this.pos+=4;
	tempBuffer.writeUInt32LE(this.importantColors,this.pos);this.pos+=4;

	for (var index in this.palette)
	{
		var paletteObj = this.palette[index];
		tempBuffer.writeUInt8(paletteObj.r , this.pos); this.pos += 1;
		tempBuffer.writeUInt8(paletteObj.g , this.pos); this.pos += 1;
		tempBuffer.writeUInt8(paletteObj.b , this.pos); this.pos += 1;
		tempBuffer.writeUInt8(paletteObj.a , this.pos); this.pos += 1;
	}
	
	
	var i=0;
	var rowBytes = this.width;

	
	for (var y = this.height - 1; y >= 0; y--)
	//for (var y = 0; y < this.height; y ++)
	{
		for (var x = 0; x < this.width; x++)
		{
			var p = this.pos+y*rowBytes+x;
			tempBuffer[p]  = this.buffer[i++];
			
		}
	}
//	for (var y = this.height - 1; y >= 0; y--){
//		for (var x = 0; x < this.width; x++){
//			var p = this.pos+y*rowBytes+x;
//			tempBuffer[p]  = this.buffer[i++];//b
//			i++;
//		}
//		if(this.extraBytes>0){
//			var fillOffset = this.pos+y*rowBytes+this.width;
//			tempBuffer.fill(0,fillOffset,fillOffset+this.extraBytes);	
//		}
//	}

	return tempBuffer;
};




module.exports.encode = function(imgData, quality) {
  if (typeof quality === 'undefined') quality = 100;
 	var encoder = new BmpEncoder(imgData);
	var data = encoder.encode();
  return {
    data: data,
    width: imgData.width,
    height: imgData.height
  };
}
